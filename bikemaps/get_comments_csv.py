import csv
from bikemaps.rides.models import Ride


with open('comments_output.csv', 'w', newline='') as csv_file:
	writer = csv.DictWriter(csv_file, fieldnames=['City', 'Person', 'Number', 'Time', 'Comment'])
	writer.writeheader()

	for ride in Ride.objects.all():
		comments = ride.get_comments()
		for comment in comments:
			comment_dict = {
				'City': ride.city.name,
				'Person': ride.person.person_name,
				'Number': comment.number,
				'Time': comment.time,
				'Comment': comment.comment
			}
			writer.writerow(comment_dict)

	print("cool")


