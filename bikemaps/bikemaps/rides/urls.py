from django.conf.urls import url

from bikemaps.rides import views

urlpatterns = [
	url(r'^$', views.map),
]
