from rest_framework import routers
from .views import RideViewSet, PersonViewSet, CityViewSet

router = routers.DefaultRouter()
router.register(r'rides', RideViewSet)
router.register(r'people', PersonViewSet)
router.register(r'cities', CityViewSet)

urlpatterns = router.urls

