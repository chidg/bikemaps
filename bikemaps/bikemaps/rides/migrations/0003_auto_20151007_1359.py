# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rides', '0002_auto_20151005_1321'),
    ]

    operations = [
        migrations.AddField(
            model_name='ridemoment',
            name='elevation',
            field=models.DecimalField(null=True, max_digits=6, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='ride',
            name='city',
            field=models.ForeignKey(related_name='rides', to='rides.City'),
        ),
        migrations.AlterField(
            model_name='ride',
            name='person',
            field=models.ForeignKey(related_name='rides', to='rides.Person'),
        ),
        migrations.AlterField(
            model_name='ridemoment',
            name='ride',
            field=models.ForeignKey(related_name='moments', to='rides.Ride'),
        ),
    ]
