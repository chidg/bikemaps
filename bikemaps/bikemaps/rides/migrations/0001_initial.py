# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'City',
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('real_name', models.CharField(max_length=50)),
                ('fake_name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'Person',
                'verbose_name_plural': 'People',
            },
        ),
        migrations.CreateModel(
            name='Ride',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField()),
                ('city', models.ForeignKey(to='rides.City')),
                ('person', models.ForeignKey(to='rides.Person')),
            ],
            options={
                'verbose_name': 'Ride',
                'verbose_name_plural': 'Rides',
            },
        ),
        migrations.CreateModel(
            name='RideMoment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('lon', models.DecimalField(max_digits=9, decimal_places=6)),
                ('time', models.DateTimeField()),
                ('comment', models.TextField(max_length=500, null=True)),
                ('ride', models.ForeignKey(to='rides.Ride')),
            ],
            options={
                'verbose_name': 'RideMoment',
                'verbose_name_plural': 'RideMoments',
            },
        ),
    ]
