from django.contrib.gis.geos.point import Point
from django.contrib.gis.geos.linestring import LineString
from django.db import models
from django.contrib.gis.db import models as gis_models
from geopy.distance import great_circle

# Create your models here.
class City(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "City"
        verbose_name_plural = "Cities"

    def people(self):
        return Person.objects.filter(rides__city=self).distinct()

    def __str__(self):
        return self.name


class Person(models.Model):
    person_name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"

    def __str__(self):
        return self.person_name


class Ride(models.Model):
    person = models.ForeignKey(Person, related_name='rides')
    city = models.ForeignKey(City, related_name='rides')
    time = models.DateTimeField()

    class Meta:
        verbose_name = "Ride"
        verbose_name_plural = "Rides"

    def __str__(self):
        return "%s in %s at %s" %(self.person, self.city, self.time)

    def get_comments(self):
        return self.moments.exclude(comment__isnull=True)

    def get_linestring(self):
        return LineString([m.point for m in self.moments.all()])

    def get_distance(self):
        distance = 0
        i = 1
        for m in self.moments.all():
            if m == self.moments.last():
                break
            distance += great_circle(m.point.coords, self.moments.all()[i].point.coords).meters
            i += 1
        return distance

    def get_duration(self):
        return self.moments.latest('time').time - self.moments.earliest('time').time


class RideMoment(gis_models.Model):
    ride = models.ForeignKey(Ride, related_name='moments')
    lat = gis_models.FloatField()
    lon = gis_models.FloatField()
    time = models.DateTimeField()
    elevation = models.DecimalField(max_digits=6, decimal_places=3, null=True)
    comment = models.TextField(null=True, max_length=500)

    class Meta:
        verbose_name = "RideMoment"
        verbose_name_plural = "RideMoments"
        ordering = ('time',)

    @property
    def number(self):
        index = list(map(lambda x: x.id, self.ride.get_comments())).index(self.id)
        if index is not None:
            return index + 1

    def __str__(self):
        return "{} at {}, {}".format(self.ride.person, self.lat, self.lon)

    @property
    def point(self):
        return Point(self.lon, self.lat)
