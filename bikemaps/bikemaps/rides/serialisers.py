from rest_framework import serializers
from .models import Ride, RideMoment, Person, City
from django.contrib.gis.geos import Point
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometryField, GeometrySerializerMethodField


class RideMomentSerialiser(GeoFeatureModelSerializer):
    point = GeometrySerializerMethodField()

    class Meta:
        model = RideMoment
        geo_field = 'point'
        fields = ('id', 'point', 'time')

    def get_point(self, obj):
        return Point(obj.lon, obj.lat)

class RideCommentSerialiser(RideMomentSerialiser):

    class Meta(RideMomentSerialiser.Meta):
        fields = RideMomentSerialiser.Meta.fields + ('comment', 'number')

        

class SimpleRideMomentSerialiser(serializers.ModelSerializer):
    class Meta:
        model = RideMoment
        fields = ('id', 'comment')


class RidePathSerialiser(GeoFeatureModelSerializer):
    linestring = GeometryField(source="get_linestring")

    class Meta:
        model = Ride
        fields = ('linestring', )
        geo_field = 'linestring'


class RideCommentSetSerialiser(GeoFeatureModelSerializer):
    comments = RideCommentSerialiser(source='get_comments', many=True)

    class Meta:
        model = Ride
        fields = ('comments',)
        geo_field = 'comments'


class RideSerialiser(serializers.ModelSerializer):
    # path = RidePathSerialiser(source='*')
    # comments = RideCommentSetSerialiser(source='*')
    comments = RideCommentSerialiser(source='get_comments', many=True)
    path = GeometryField(source="get_linestring")

    class Meta:
        model = Ride
        fields = ('id', 'time', 'path', 'comments', 'person', 'city')


class PersonSerialiser(serializers.ModelSerializer):
    # cities = serializers.SerializerMethodField()

    class Meta:
        model = Person
        fields = ('id', 'person_name')

    # def get_cities(self, obj):
    #     return set([r.city.name for r in obj.rides.all()])


class CitySerialiser(serializers.ModelSerializer):
    people = PersonSerialiser(many=True)

    class Meta:
        model = City
        fields = ('name', 'people')



