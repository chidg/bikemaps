from django.shortcuts import render
from rest_framework import viewsets
from .serialisers import RideSerialiser, PersonSerialiser, CitySerialiser
from .models import Ride, Person, City


class RideViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing rides.
    """
    queryset = Ride.objects.all()
    serializer_class = RideSerialiser

    def get_queryset(self):
        queryset = super().get_queryset()
        city = self.request.query_params.get('city', None)
        if city is not None:
            queryset = queryset.filter(city__name=city)
        person = self.request.query_params.get('person', None)

        if person is not None:
            queryset = queryset.filter(person__person_name=person)

        return queryset


class PersonViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerialiser

    def get_queryset(self):
        queryset = super().get_queryset()
        city = self.request.query_params.get('city', None)
        if city is not None:
            queryset = queryset.filter(rides__city__name=city)
        print(queryset)
        return queryset


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerialiser


def map(request):
    return render(request, 'rides/base.html', {})
