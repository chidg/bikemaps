from datetime import datetime
from django.db import transaction
from django.utils import timezone
from django.core.management.base import BaseCommand
from django.contrib.gis.geos.point import Point
from bikemaps.rides.models import City, Ride, Person, RideMoment
import csv, os, gpxpy

"""
This version of the command loads all data into memory and then intercalates the ride moments before
attempting to align comments with locations
"""


class Command(BaseCommand):
    # missing_args_message = "This command must be run with at least one argument, the name of the person whose ride we'll be processing"
    path = 'sources'

    def add_arguments(self, parser):
        # Positional arguments
        # parser.add_argument('names', nargs='+', type=str)

        # Named (optional) arguments
        parser.add_argument('--city',
            action='store',
            dest='city',
            default=False,
            help='Specify which city')

    def handle(self, *args, **options):
        city = options['city']

        allrides = {}

        #  Load csv into memory
        print('running script for %s' % city)
        try:
            self.path = os.path.join('sources', city)
        except AttributeError:  # city arg probably not given
            print("Give a --city")
            exit()
        csv_file = os.path.join(self.path, city+' Rides.csv')
        try:
            with open(csv_file, newline='', encoding='utf-8', errors='surrogateescape') as cfile:
                csv_data = csv.DictReader(cfile, delimiter=',', quotechar='"')
                required_columns = ['city', 'person_name', 'ride',  'time',  'comment']
                for column in required_columns:
                    try:
                        if column not in csv_data.fieldnames:
                            print("It looks like your csv file for %s did not contain a %s column" %(city, column))
                            exit()
                    except csv.Error as e:
                        print('there was an error: %s' % e)
                        print(csv_data.fieldnames)
                self.load_rides(csv_data, city)

        except IOError as e: # File was not present
            print("This won't work without a properly formatted csv file for %s" % city)
            print(e)
            exit()
        print ('i guess that worked')

    def load_rides(self, csv_data, city):
        # Create City object
        city_instance = City.objects.get_or_create(name=city)[0]

        counter = 0

        current_ride = False

        previous_row = {'person_name': '', 'ride': 0}

        ride_data = {}
        """
        ride_data is a dictionary which will have the shape:
        {
            person_name: [
                [
                    {   # a ride point extracted from a gpx file
                        'time': tpoint.time,
                        'lat': lat,
                        'lon': lon,
                        'elevation': tpoint.elevation
                    {
                        # a ride point containing a comment, extracted from a csv
                        'comment': comment,
                        'time': csv_time
                    },
                    {
                        # a ride point extracted from a gpx file
                        'time': tpoint.time,
                        'lat': lat,
                        'lon': lon,
                        'elevation': tpoint.elevation
                    },
                    {
                        #another ride point...
                    },
                    {...}
                ],
                [ride_2 moments...],
                [ride_3 moments...]
            ]
        }
        """

        # Suck up all the data
        previous_person_name = ''
        for row in csv_data:
            person_name = row.get('person_name')
            city = row.get('city')
            ride_no = int(row.get('ride'))
            time = row.get('time')
            comment = row.get('comment')
            if person_name != previous_person_name:
                Person.objects.get_or_create(person_name=person_name)[0]

            try:
                t = datetime.strptime(time, "%M:%S")
            except ValueError: # This happens when the time is given as HH:MM:SS but it is actually referring to MM:SS
                time_string = time
                stripped_time = time_string.rsplit(':', 1)[0]
                t = datetime.strptime(stripped_time, "%M:%S")

            if comment.lower().strip() == 'start':
                csv_start_time = t

            csv_elapsed_time = t - csv_start_time
            moment = {'comment': comment, 'elapsed_time': csv_elapsed_time}
            try:
                ride_data[person_name][ride_no - 1].append(moment)

            except KeyError as e: # name doesn't exist yet
                ride_data[person_name] = [[moment]]
                print('Starting ride for {}'.format(e))
            except IndexError: # Name exists but ride doesn't
                ride_data[person_name].append([moment])

            previous_person_name = person_name


        # Now loop through gpx files and add that data
        with transaction.atomic():
            for person, rides in ride_data.items():
                person_instance = Person.objects.get(person_name=person)
                i = 0
                for ride in rides:
                    ride = sorted(ride, key=lambda k: k['elapsed_time'])
                    ride_moments = []
                    i += 1
                    print('opening gpx file for %s' % person)
                    try:
                        gpx_file = open(os.path.join(self.path, person+'.gpx'), 'r')
                    except FileNotFoundError:
                        try:
                            gpx_file = open(os.path.join(self.path, person+str(i)+'.gpx'), 'r')
                        except FileNotFoundError:
                            print("there was no gpx file for %s" % (person))
                            continue
                    gpx = gpxpy.parse(gpx_file)
                    base_time = timezone.make_aware(gpx.tracks[0].segments[0].points[0].time)

                    print('opened gpx file for %s' % person)
                    for track in gpx.tracks:
                        for segment in track.segments:
                            for tpoint in segment.points:
                                gps_time = timezone.make_aware(tpoint.time)
                                gps_elapsed_time = gps_time - base_time
                                moment = {
                                    # 'elapsed_time': gps_elapsed_time,
                                    'time': gps_time,
                                    'lat': tpoint.latitude,
                                    'lon': tpoint.longitude,
                                    # 'point': Point((tpoint.longitude, tpoint.latitude)),
                                    'elevation': tpoint.elevation,
                                }
                                if ride:
                                    if ride[0]['elapsed_time'] <= gps_elapsed_time:
                                        moment['comment'] = ride[0]['comment']
                                        ride.pop(0)
                                ride_moments.append(moment)

                    # We've finished adding gps points to it
                    ride_instance = Ride.objects.get_or_create(person=person_instance, city=city_instance, time=base_time)[0]
                    if hasattr(ride_moments[0], 'comment') and ride_moments[0]['comment'].lower() == 'start':
                        ride_moments.pop(0)

                    for moment in ride_moments:
                        RideMoment.objects.get_or_create(ride=ride_instance, **moment)[0]
                    print('created ride {}'.format(ride_instance))
            print('Finished city')


