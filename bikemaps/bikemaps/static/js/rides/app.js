angular.module('bikeMapApp', ['ridesResources', 'ridesDirectives'])
.config(function($locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
})
.controller('BikeMapController',
  ['$scope', '$q', '$location', 'Ride', 'City', function($scope, $q, $location, Ride, City) {
  var defaultCity = 'Melbourne';
  var bikemap = $scope;

  var CSS_COLOR_NAMES = ["Aquamarine","Azure","Beige","Bisque","Black","BlanchedAlmond","Blue","BlueViolet","Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod","DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen","DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick","FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink","IndianRed","Indigo","Ivory","Khaki","Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan","LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen","LightSkyBlue","LightSlateGray","LightSlateGrey","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid","MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue","MintCream","MistyRose","Moccasin","NavajoWhite","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen","PaleTurquoise","PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon","SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen","SteelBlue","Tan","Teal","Thistle","Tomato","Turquoise","Violet","Wheat","White","WhiteSmoke","Yellow","YellowGreen"];

  mapboxgl.accessToken = 'pk.eyJ1IjoiY2hpZCIsImEiOiJzTFFIZU5BIn0.WSsXUs4Bb7_5bxPaGXYgKw';
  // var homeBounds = [[226.93359375, -4.390228926463384], [58.18359375, -45.95114968669139]]
  var map = new mapboxgl.Map({
    container: 'map',
    zoom: 12,
    center: [144.9, -37.8],
    style: 'mapbox://styles/mapbox/bright-v9',
    hash: false
  });

  bikemap.loading = true;
  City.query({}, function (data) {
    bikemap.cities = data;
  });

  bikemap.doRideQuery = function(params) {
    ga('send', 'pageview', $location.search());

    Ride.query(params, function(data) {
      bikemap.loading = false;

      // Remove existing layers
      var existingLayers = Object.keys(map.style._layers).filter(function(key) {
        return key.startsWith('comments_') || key.startsWith('ride_')
      });
      angular.forEach(existingLayers, function(value, key) {
        map.removeLayer(value);
        map.removeSource(value);
      });

      // Add new stuff
      var bounds = new mapboxgl.LngLatBounds();

      angular.forEach(data, function(value, key) {
        var id = 'comments_' + value.id;
        map.addSource(id, {
          'type': 'geojson',
          'data':  value.comments
        });
        map.addLayer({
          "id": id,
          "type": "symbol",
          "source": id,
          "layout": {
            "icon-image": 'null',
            "text-field": "{number}",
            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
            "text-offset": [0, 2],
            "text-anchor": "left",
            "text-size": 14
          }
        });

        map.on('click', id, function (e) {
          new mapboxgl.Popup()
          .setLngLat(e.lngLat)
          .setHTML(e.features[0].properties.comment)
          addTo(map);
        })

        map.on('mouseenter', id, function() {
          map.getCanvas().style.cursor = 'pointer';
        });

        map.on('mouseleave', id, function() {
          map.getCanvas().style.cursor = '';
        });

        //add lines
        var rideId = 'ride_' + value.id;
        map.addSource(rideId, {
          type: 'geojson',
          data: {
            type: 'Feature',
            geometry: value.path
          }
        });

        map.addLayer({
          'id': rideId,
          'type': 'line',
          'source': rideId,
          'layout': {
            'line-join': 'round',
            'line-cap': 'round'
          },
          'paint': {
            'line-color': CSS_COLOR_NAMES[value.id],
            'line-width': 4
          }
        });
        const line = turf.lineString(value.path.coordinates);
        const bbox = turf.bbox(line);

        bounds.extend(new mapboxgl.LngLatBounds([bbox[0], bbox[1]], [bbox[2], bbox[3]]));

        if (key === data.length - 1) {
          map.fitBounds(bounds, {padding: 50});
        }
      });

    });
  };

  var params = {city: defaultCity};

  params = angular.extend(params, $location.search());
  bikemap.city = {name: params.city};
  bikemap.person = {name: params.person};

  map.on('load', function() {
    bikemap.doRideQuery(params);
  });

  City.query({}, function (data) {
    bikemap.cities = data;
  });

  bikemap.$on('citySelected', function (e, city) {
    bikemap.city = city;
    bikemap.doRideQuery({city: city.name});
    $location.search({city: city.name});
    delete bikemap.person;
  });

  bikemap.$on('personSelected', function (e, person, city) {
    bikemap.doRideQuery({person: person.person_name, city: city.name});
    $location.search({'person': person.person_name, 'city': city.name});
    bikemap.person = person;
  });

  bikemap.$on('commentSelected', function (e, comment) {
    map.setView(comment.getPoint(), 18);
    // comment.focusMarker();
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  });

}]);
