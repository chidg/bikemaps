var ridesResources = angular.module('ridesResources', ['ngResource']);

ridesResources.config(['$resourceProvider', function($resourceProvider) {
  // Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = false;
}]);


ridesResources.factory('Ride', function($resource){
	return $resource('/api/rides/', {}, {
		'query':  {method:'GET', isArray: true},
	});
});

ridesResources.factory('City', function($resource){
    return $resource('/api/cities', {}, {
        'query':  {method:'GET', isArray: true},
    });
});
