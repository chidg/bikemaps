angular.module('ridesDirectives', [])
    .directive('cityList', function() {
        return {
            type: 'e',
            //replace: true,
            scope: {
                cities: '='
            },
            link: function(scope) {
                scope.selectCity = function(city) {
                    scope.$emit('citySelected', city);
                }
            },
            template: '<ul class="nav navbar-nav navbar-right">' +
                        '<li ng-repeat="city in cities" ><i class="fa fa-bicycle"></i> {{ city.name }}</li>' +
                        //'<people-list ng-repeat="city in cities" people="city.people" city="city">' +
                        '</ul>'
        }
    })
    .directive('peopleList', function() {
        return {
            type: 'e',
            replace: true,
            scope: {
                city: '=',
                person: '='
            },
            link: function(scope) {
                scope.selectPerson = function(person, city) {
                    scope.$emit('personSelected', person, city);
                };
                scope.selectCity = function(city) {
                    scope.$emit('citySelected', city);
                };
            },
            templateUrl: "static/js/rides/templates/city-dropdown.html"
        }
    })
    .directive('rideCommentsList', function() {
        return {
            type: 'e',
            scope: {
                comments: '=',
                person: '=',
                city: '='
            },
            link: function(scope) {
                scope.selectComment = function(comment) {
                    scope.$emit('commentSelected', comment);
                }
            },
            template:
                '<ul class="list-group">' +
                '  <li class="list-group-item list-group-item"> {{ person.name }} - {{ city.name }} - {{ comments[0].time|date }}' +
                '  <li ng-repeat="comment in comments" class="list-group-item" ng-click="selectComment(comment)">' +
                '   <span class="small"> {{ comment.time }}</span>' +
                '   <p>{{ comment.comment }}</p>' +
                '   </li>' +
                '</ul>'
        }
    });
